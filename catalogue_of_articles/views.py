from django.shortcuts import render

# Create your views here.
from rest_framework.filters import OrderingFilter
from rest_framework.viewsets import ModelViewSet
from catalogue_of_articles.models import Article, Author
from catalogue_of_articles.serializers import AuthorListSerializer, AuthorDetailSerializer, \
    ArticleDetailSerializer, ArticleListSerializer


def index(request):
    return render(request, 'catalogue_of_articles/index.html')


class ArticleViewSet(ModelViewSet):
    queryset = Article.objects.all().prefetch_related('authors')
    serializer_class = ArticleDetailSerializer
    serializer_action_classes = {
            'list': ArticleListSerializer,
    }

    def get_serializer_class(self, *args, **kwargs):
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()


class AuthorViewSet(ModelViewSet):
    queryset = Author.objects.all().prefetch_related('articles')
    serializer_class = AuthorDetailSerializer
    serializer_action_classes = {
            'list': AuthorListSerializer,
    }
    # For ordering need ?ordering=<order column> in url
    filter_backends = [OrderingFilter]
    ordering_fields = ['second_name', 'first_name']

    def get_serializer_class(self, *args, **kwargs):
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()
