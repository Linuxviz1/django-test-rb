from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin
from catalogue_of_articles.models import Article, Author


class AuthorAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ("my_order","id", "first_name", "second_name")


class ArticleAdmin(admin.ModelAdmin):
    list_display = ("id", "title")


admin.site.register(Article, ArticleAdmin)
admin.site.register(Author, AuthorAdmin)
