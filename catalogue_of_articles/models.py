from django.db import models


class Article(models.Model):
    """
    title - article title
    text - article text
    authors - for this, the authors of the name from the article are available to her
    """
    title = models.CharField(max_length=250)
    text = models.TextField()

    def __str__(self):
        return f"{self.pk}: '{self.title}'"


class Author(models.Model):
    """
    first_name
    second_name
    articles
    my_order - this is custom order of authors
    """
    first_name = models.CharField(max_length=150)
    second_name = models.CharField(max_length=150)
    articles = models.ManyToManyField(Article, related_name='authors', blank=True)
    my_order = models.PositiveSmallIntegerField(default=0, blank=False, null=False)

    order = 'second_name', 'first_name'

    class Meta:
        ordering = ['my_order', ]

    def __str__(self):
        return f"{self.first_name} {self.second_name}"
