from django.urls import path
from rest_framework.routers import SimpleRouter

from catalogue_of_articles.views import AuthorViewSet, ArticleViewSet, index

catalogue_of_articles_router = SimpleRouter()

catalogue_of_articles_router.register('author', AuthorViewSet)
catalogue_of_articles_router.register('article', ArticleViewSet)

urlpatterns = [
        path('', index, name='main'),
]

urlpatterns.extend(catalogue_of_articles_router.urls)
