from rest_framework import serializers

from .models import Author, Article


class ArticleFromAuthorDetailSerializer(serializers.ModelSerializer):
    """This support serializer for author detail pages"""

    class Meta:
        model = Article
        fields = ('id', 'title',)


class AuthorDetailSerializer(serializers.HyperlinkedModelSerializer):
    """This serializer used for /author/pk pages"""

    articles_data = ArticleFromAuthorDetailSerializer(source="articles", many=True, read_only=True)

    class Meta:
        model = Author
        fields = ('id', 'first_name', 'second_name', 'articles_data', 'articles')


class AuthorListSerializer(serializers.ModelSerializer):
    """This serializer used for /author/ page"""

    url_detail = serializers.HyperlinkedIdentityField(view_name='author-detail',
                                                      format='html',
                                                      many=False,
                                                      read_only=True)

    class Meta:
        model = Author
        fields = ['id', 'first_name', 'second_name', 'url_detail']


class AuthorFromArticleListSerializer(serializers.ModelSerializer):
    """This support serializer for /author/pk pages"""

    class Meta:
        model = Author
        fields = ('id', 'first_name', 'second_name',)


class AuthorFromArticleDetailSerializer(serializers.ModelSerializer):
    """This support serializer for /author/ page"""

    url_author = serializers.HyperlinkedIdentityField(view_name='author-detail',
                                                      format='html',
                                                      many=False,
                                                      read_only=True)

    class Meta:
        model = Author
        fields = ('id', 'first_name', 'second_name', 'url_author')


class ArticleDetailSerializer(serializers.ModelSerializer):
    """This serializer used for /article/pk pages"""
    authors_pk = serializers.ListField(child=serializers.IntegerField(min_value=0, max_value=1000), write_only=True)
    authors_data = AuthorFromArticleDetailSerializer(source="authors", many=True, read_only=True)

    class Meta:
        model = Article
        fields = ['id', 'title', 'text', 'authors_data', 'authors_pk']

    def create(self, validated_data):
        """This action called from post method,
         need add to json request field authors_pk:[int,]
          if you want crate many to many relationship or authors_pk:[] if not"""
        author_indexes = validated_data.pop('authors_pk')
        article = Article.objects.create(**validated_data)
        for author_index in author_indexes:
            article.authors.add(author_index)
        return article


class ArticleListSerializer(serializers.ModelSerializer):
    """This serializer used for /article/ page"""
    authors_name = AuthorFromArticleListSerializer(source="authors", many=True, read_only=True)

    class Meta:
        model = Article
        fields = ['id', 'title', 'authors_name', 'url_detail']

    url_detail = serializers.HyperlinkedIdentityField(view_name='article-detail',
                                                      format='html',
                                                      many=False,
                                                      read_only=True)
