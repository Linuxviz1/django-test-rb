from django.core.management import BaseCommand, CommandError
from catalogue_of_articles.management.commands.download_data_support.requests_logic import get_main_page, \
    download_and_save_data

MAIN_URL = "https://rb.ru"
NEWS_URL = "https://rb.ru/news/"


class Command(BaseCommand):
    """
    The team downloads 30 articles from the rb.ru website.
    First comes the parsing of the rb.ru/news page to get links to articles.
    Then each article is parsed to obtain the author's name,
                                                       title,
                                            and article text.
    Since the body of the article is quite voluminous and the requests are executed without asynchrony,
     the function can take a LOT of time, up to 2 minutes.
    """
    help = 'Create authors and articles in data base [generatedata (number > 3)]'

    def handle(self, *args, **options):
        try:
            news_list_urls = get_main_page(NEWS_URL, MAIN_URL)
            download_and_save_data(news_list_urls)
            self.stdout.write(self.style.SUCCESS(f'success generated'))
        except Exception:
            raise CommandError("error")
