from random import randint

SECOND_NAME = (
        'Smith',
        'Johnson',
        'Williams',
        'Brown',
        'Jones',
        'Miller',
        'Davis',
        'Garcia',
        'Rodriguez',
        'Wilson',
        'Martinez',
        'Anderson',
        'Taylor',
        'Thomas',
        'Hernandez',
        'Moore',
        'Martin',
        'Jackson',
        'Thompson',
        'White',
        'Lopez',
        'Lee',
        'Gonzalez',
        'Harris',
        'Clark',
        'Lewis',
        'Robinson',
        'Walker',
        'Perez',
        'Hall',
        'Young',
)


def second_name():
    return SECOND_NAME[randint(0, len(SECOND_NAME) - 1)]
