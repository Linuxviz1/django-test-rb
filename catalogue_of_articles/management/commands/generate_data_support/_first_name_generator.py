from random import randint

FIRST_NAME = (
        'Samuel',
        'Jack',
        'Joseph',
        'Harry',
        'Alfie',
        'Jacob',
        'Thomas',
        'Charlie',
        'Oscar',
        'James',
        'William',
        'Joshua',
        'George',
        'Ethan',
        'Noah',
        'Archie',
        'Henry',
        'Leo',
        'John',
        'Oliver',
        'David',
        'Ryan',
        'Dexter',
        'Connor',
        'Albert',
        'Austin',
        'Stanley',
        'Theodore',
        'Owen',
        'Caleb',
)


def first_name():
    return FIRST_NAME[randint(0, len(FIRST_NAME) - 1)]
