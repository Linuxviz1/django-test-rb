from random import randint
from django.utils.lorem_ipsum import words


def title():
    count = randint(2, 6)
    return words(count, False)
