from catalogue_of_articles.models import Article, Author


def _split_author_name(author_name: str) -> tuple:
    """
    Returns the author's first and last name,
    may parse incorrectly if surname is specified first.
    In addition, if the full name consists of more than two words,
    then all additional words will be added to the last name.
    """
    author_name = author_name.split()
    return author_name[0], ' '.join(author_name[1:])


def save(author_name: str, title: str, text: str):
    """
    The function creates and saves an article to the database,
     as well as creates and saves or receives an instance (if already created),
      the author and adds the author-article link.
    """
    first_name, second_name = _split_author_name(author_name)
    article = Article.objects.create(
            title=title,
            text=text,
    )
    author, is_created = Author.objects.get_or_create(
            first_name=first_name,
            second_name=second_name,
    )
    author.articles.add(article)
