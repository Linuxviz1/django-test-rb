from re import sub
import requests
from bs4 import BeautifulSoup

from catalogue_of_articles.management.commands.download_data_support.save_logic import save


def get_main_page(news_url: str, main_url: str) -> list:
    """The function gets the url of the main page and returns 30 links to articles"""
    r = requests.get(news_url)
    parser = BeautifulSoup(r.text, features="html.parser")
    news_items = parser.find_all('div', {'class': 'news-item__image'})
    return [f"{main_url}{link.find('a').get('href')}" for link in news_items]


def download_and_save_data(news_list_urls):
    """The function receives a list of absolute URLs for news,
     parses the page by highlighting the title,
      text and author's name and passes them to the save function. """
    for link in news_list_urls:
        r = requests.get(link)
        parser = BeautifulSoup(r.text, features="html.parser")
        author_name = parser.find('div', {'id': 'article-feed'}) \
            .find('span', {'class': 'article-header__author-name'}) \
            .find('a').text
        title = parser.find('h1', {'class': 'article-header__rubric-title'}).text
        intro = parser.find('div', {'itemtype': "https://schema.org/NewsArticle"}).find('p').text
        content = parser.find('div', {'class': 'article__content-block abv', 'itemprop': 'articleBody'}).get_text()
        text = sub(r'\n+', '\n', f'{intro}{content}')
        save(author_name, title, text)
