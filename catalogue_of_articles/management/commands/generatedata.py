from random import choices, randint

from django.core.management import BaseCommand, CommandError

from catalogue_of_articles.management.commands.generate_data_support._first_name_generator import first_name
from catalogue_of_articles.management.commands.generate_data_support._second_name_generator import second_name
from catalogue_of_articles.management.commands.generate_data_support._text_generator import text
from catalogue_of_articles.management.commands.generate_data_support._title_generator import title
from catalogue_of_articles.models import Article, Author

AUTHORS_NUMBER = 20
ARTICLES_NUMBER = 30


class Command(BaseCommand):
    """
    to use: python3 manage.py generatedata (number)
    Functions create Authors and Articles objects in db.
    Function need more than 3 Authors. Articles can have 1-3 authors.
    For generation texts and titles using standard lorem function.
    """
    help = 'Create authors and articles in data base [generatedata (number > 3)]'

    def add_arguments(self, parser):
        parser.add_argument('number', type=int, nargs='?', default=AUTHORS_NUMBER)

    def handle(self, *args, **options):
        authors = []
        if options['number'] < 3:
            raise CommandError(f'Author number must more than 0')
        else:
            self.stdout.write(self.style.SUCCESS(f'start generation'))
            for _ in range(options['number']):
                generated_first_name = first_name()
                generated_second_name = second_name()
                try:
                    author = Author.objects.create(
                            first_name=generated_first_name,
                            second_name=generated_second_name,
                    )
                    authors.append(author)
                except Exception:
                    raise CommandError(f'Creating Author {_}: {first_name} {second_name} stop process')
            self.stdout.write(self.style.SUCCESS(f'{options["number"]} authors success generated'))
            for _ in range(ARTICLES_NUMBER):
                generated_title = title()
                generated_text = text()
                count_of_authors = randint(1, 3)
                choice_authors = choices(authors, k=count_of_authors)
                try:
                    article = Article.objects.create(
                            title=generated_title,
                            text=generated_text,
                    )
                    article.authors.add(*choice_authors)
                except Exception:
                    raise CommandError(f'Creating Article {_}: {generated_title} {generated_text} stop process')
            self.stdout.write(self.style.SUCCESS(f'{ARTICLES_NUMBER} articles success generated'))
