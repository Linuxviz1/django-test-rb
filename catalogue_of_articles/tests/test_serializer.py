from django.test import TestCase
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory

from catalogue_of_articles.models import Article, Author
from catalogue_of_articles.serializers import AuthorListSerializer, AuthorDetailSerializer, ArticleListSerializer, \
    ArticleDetailSerializer


class AuthorSerializerTestCase(TestCase):
    def setUp(self) -> None:
        self.author_1 = Author.objects.create(first_name='petr', second_name='letor')
        self.article_1 = Article.objects.create(title="Some title", text='jhon')
        self.author_1.articles.add(self.article_1)

    def test_author_list(self):
        authors = Author.objects.all().prefetch_related('articles')
        factory = APIRequestFactory()
        request = factory.get('url')
        serializer_context = {
                'request': Request(request),
        }
        serializer_data = AuthorListSerializer(authors, many=True, context=serializer_context).data
        expected_data = [
                {
                        'id':          self.author_1.id,
                        "first_name":  'petr',
                        "second_name": 'letor',
                        'url_detail':  f'http://testserver/author/{self.author_1.id}/'

                }, ]
        self.assertEqual(expected_data, serializer_data)

    def test_author_detail(self):
        authors = Author.objects.all().prefetch_related('articles')
        factory = APIRequestFactory()
        request = factory.get('url')
        serializer_context = {
                'request': Request(request),
        }
        serializer_data = AuthorDetailSerializer(authors, many=True, context=serializer_context).data
        expected_data = [
                {
                        'id':            self.author_1.pk,
                        'first_name':    'petr',
                        'second_name':   'letor',
                        'articles_data': [
                                {
                                        'id':    self.article_1.pk,
                                        'title': 'Some title',
                                },
                        ],
                        'articles':      [f'http://testserver/article/{self.article_1.pk}/'],
                }, ]
        self.assertEqual(expected_data, serializer_data)


class ArticleSerializerTestCase(TestCase):
    def setUp(self) -> None:
        self.author_1 = Author.objects.create(first_name='petr', second_name='letor')
        self.article_1 = Article.objects.create(title="Some title", text='jhon')
        self.author_1.articles.add(self.article_1)

    def test_article_list(self):
        articles = Article.objects.all().prefetch_related('authors')
        factory = APIRequestFactory()
        request = factory.get('url')
        serializer_context = {
                'request': Request(request),
        }
        serializer_data = ArticleListSerializer(articles, many=True, context=serializer_context).data
        expected_data = [
                {
                        'id':           self.article_1.id,
                        'title':        'Some title',
                        'authors_name': [
                                {
                                        'id':          self.author_1.pk,
                                        'first_name':  'petr',
                                        'second_name': 'letor'
                                },
                        ],
                        'url_detail':   f'http://testserver/article/{self.article_1.id}/'

                }, ]
        self.assertEqual(expected_data, serializer_data)

    def test_author_detail(self):
        articles = Article.objects.all().prefetch_related('authors')
        factory = APIRequestFactory()
        request = factory.get('url')
        serializer_context = {
                'request': Request(request),
        }
        serializer_data = ArticleDetailSerializer(articles, many=True, context=serializer_context).data
        expected_data = [
                {
                        'id':           self.article_1.id,
                        'title':        'Some title',
                        'text':         'jhon',
                        'authors_data': [
                                {
                                        'id':          self.author_1.pk,
                                        'first_name':  'petr',
                                        'second_name': 'letor',
                                        'url_author':  f'http://testserver/author/{self.article_1.id}/',
                                },
                        ],

                }, ]
        self.assertEqual(expected_data, serializer_data)
