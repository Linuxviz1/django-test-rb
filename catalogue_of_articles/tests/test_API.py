import json

from django.urls import reverse
from rest_framework import status
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory
from rest_framework.test import APITestCase

from catalogue_of_articles.models import Author, Article
from catalogue_of_articles.serializers import ArticleListSerializer, AuthorListSerializer


class ArticleAPITestCase(APITestCase):
    def setUp(self):
        self.author_1 = Author.objects.create(first_name='test_username', second_name='test_second_name')
        self.article_1 = Article.objects.create(title="Some title", text="Some test text")
        self.article_1.authors.add(self.author_1)

    def test_get(self):
        url = reverse("article-list")
        response = self.client.get(url)
        factory = APIRequestFactory()
        request = factory.get('url')
        serializer_context = {
                'request': Request(request),
        }
        queryset = Article.objects.all().prefetch_related('authors')
        serializer_data = ArticleListSerializer(queryset, many=True, context=serializer_context).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data)

    def test_update(self):
        url = reverse("article-detail", args=(self.article_1.pk,))
        response = self.client.put(url,
                                   data=json.dumps(
                                           {
                                                   'title':      'new title',
                                                   'text':       'new text',
                                                   'authors_pk': [],
                                           }
                                   ), content_type='application/json'
                                   )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.article_1.refresh_from_db()
        self.assertEqual('new title', self.article_1.title)
        self.assertEqual('new text', self.article_1.text)

    def test_partial_update(self):
        url = reverse("article-detail", args=(self.article_1.pk,))
        response = self.client.patch(url,
                                     data=json.dumps(
                                             {
                                                     'title': 'new title',
                                             }
                                     ), content_type='application/json'
                                     )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.article_1.refresh_from_db()
        self.assertEqual('new title', self.article_1.title)

    def test_create(self):
        self.assertEqual(1, Article.objects.all().count())
        url = reverse("article-list")
        response = self.client.post(url,
                                    data=json.dumps(
                                            {
                                                    'text':       'textz',
                                                    'title':      'about trees',
                                                    'authors_pk': [self.author_1.pk],
                                            }
                                    ), content_type='application/json'
                                    )
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(2, Article.objects.all().count())
        self.assertEqual(Author.objects.get(pk=self.author_1.pk), Article.objects.last().authors.all()[0])

    def test_delete(self):
        url = reverse("article-detail", args=(self.article_1.pk,))
        self.assertEqual(1, Article.objects.all().count())
        response = self.client.delete(url)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(0, Article.objects.all().count())
        self.assertEqual(False, Article.objects.filter(pk=self.article_1.pk).exists())


class AuthorAPITestCase(APITestCase):
    def setUp(self):
        self.author_1 = Author.objects.create(first_name='test_username', second_name='test_second_name')
        self.article_1 = Article.objects.create(title="Some title", text="Some test text")
        self.article_1.authors.add(self.author_1)

    def test_get(self):
        url = reverse("author-list")
        response = self.client.get(url)
        factory = APIRequestFactory()
        request = factory.get('url')
        serializer_context = {
                'request': Request(request),
        }
        queryset = Author.objects.all().prefetch_related('articles')
        serializer_data = AuthorListSerializer(queryset, many=True, context=serializer_context).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data)

    def test_update(self):
        url = reverse("author-detail", args=(self.author_1.pk,))
        response = self.client.put(url,
                                   data=json.dumps(
                                           {
                                                   'first_name':  'new first name',
                                                   'second_name': 'new second name',
                                                   'articles':    [],
                                           }
                                   ), content_type='application/json'
                                   )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.author_1.refresh_from_db()
        self.assertEqual('new first name', self.author_1.first_name)
        self.assertEqual('new second name', self.author_1.second_name)

    def test_partial_update(self):
        url = reverse("author-detail", args=(self.author_1.pk,))
        response = self.client.patch(url,
                                     data=json.dumps(
                                             {
                                                     'first_name': 'new name',
                                             }
                                     ), content_type='application/json'
                                     )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.author_1.refresh_from_db()
        self.assertEqual('new name', self.author_1.first_name)

    def test_create(self):
        self.assertEqual(1, Author.objects.all().count())
        url = reverse("author-list")
        response = self.client.post(url,
                                    data=json.dumps(
                                            {
                                                    'first_name':  'new title',
                                                    'second_name': 'new text',
                                            }
                                    ), content_type='application/json'
                                    )
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(2, Author.objects.all().count())

    def test_delete(self):
        url = reverse("author-detail", args=(self.author_1.pk,))
        self.assertEqual(1, Author.objects.all().count())
        response = self.client.delete(url)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(0, Author.objects.all().count())
        self.assertEqual(False, Author.objects.filter(pk=self.author_1.pk).exists())
