from django.core.management import call_command
from django.test import TestCase

from catalogue_of_articles.management.commands.download_data_support.save_logic import _split_author_name, save
from catalogue_of_articles.management.commands.generate_data_support._first_name_generator import first_name, FIRST_NAME
from catalogue_of_articles.management.commands.generate_data_support._second_name_generator import second_name, \
    SECOND_NAME
from catalogue_of_articles.management.commands.generate_data_support._title_generator import title
from catalogue_of_articles.models import Author, Article


class GenerateDataTestCase(TestCase):
    def test_generate_name(self):
        self.assertTrue(first_name() in FIRST_NAME)

    def test_title_generate(self):
        self.assertTrue(len(title().split()) in (2, 3, 4, 5, 6))

    def test_title_generat2(self):
        self.assertTrue(len(title().split()) in (2, 3, 4, 5, 6))

    def test_generate_second_name(self):
        self.assertTrue(second_name() in SECOND_NAME)

    def test_run(self):
        author_count = Author.objects.count()
        article_count = Article.objects.count()
        self.assertEqual(0, author_count)
        self.assertEqual(0, article_count)
        call_command('generatedata')
        author_count = Author.objects.count()
        article_count = Article.objects.count()
        self.assertEqual(20, author_count)
        self.assertEqual(30, article_count)


class DownloadDataRBTestCase(TestCase):
    def test_split(self):
        self.x = 'Vlad Dyatlov'
        self.x_1 = 'Oleg Alexandrovich Petrov'
        self.assertEqual(('Vlad', 'Dyatlov'), _split_author_name(self.x))
        self.assertEqual(('Oleg', 'Alexandrovich Petrov'), _split_author_name(self.x_1))

    def test_save(self):
        author_name = 'oleg redkin'
        title = 'title'
        text = 'some text'
        save(author_name, title, text)
        self.assertEqual('oleg', Author.objects.last().first_name)
        self.assertEqual('redkin', Author.objects.last().second_name)
        self.assertEqual('title', Article.objects.last().title)
        self.assertEqual('some text', Article.objects.last().text)
        self.assertEqual('title', Author.objects.last().articles.last().title)

    pass
    # def test_run(self):
    #     author_count = Author.objects.count()
    #     article_count = Article.objects.count()
    #     self.assertEqual(0, author_count)
    #     self.assertEqual(0, article_count)
    #     call_command('downloaddatarb')
    #     author_count = Author.objects.count()
    #     article_count = Article.objects.count()
    #     self.assertTrue(author_count > 0)
    #     self.assertTrue(author_count < 31)
    #     self.assertEqual(30, article_count)
