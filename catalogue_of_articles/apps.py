from django.apps import AppConfig


class CatalogueOfArticlesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'catalogue_of_articles'
