var generator = new Vue({
    el: '#element',
    data: {
        status: '',
        elements: [],
        newArticleAuthor: [],
        newArticleTitle: '',
        newArticleText: '',
        csrf: csrf
    },
    methods: {
        getArticleList: function () {
            const vm = this;
            axios.get('/article/').then(
                function (response) {
                    vm.status = 'ArticleList'
                    vm.elements = response.data
                })
        },
        getAuthorList: function () {
            const vm = this;
            axios.get('/author/').then(
                function (response) {
                    vm.status = 'AuthorList'
                    vm.elements = response.data
                })
        },
        postNewArticle: function () {
            const vm = this;
            axios.get('/author/').then(
                function (response) {
                    vm.status = 'NewArticle'
                    vm.elements = response.data
                })
        },
        detailAuthor: function (url) {
            const vm = this;
            axios.get(url).then(
                function (response) {
                    vm.status = 'AuthorDetail'
                    vm.elements = response.data
                })
        },
        detailArticle: function (url) {
            const vm = this;
            axios.get(url).then(
                function (response) {
                    vm.status = 'ArticleDetail'
                    vm.elements = response.data
                })
        },
        createNewArticle: function () {
            const vm = this;
            axios({
                url: 'http://127.0.0.1:8000/article/',
                method: 'POST',
                data: {
                    title: vm.newArticleTitle,
                    text: vm.newArticleText,
                    authors_pk: vm.newArticleAuthor
                },
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    'X-CSRFToken': vm.csrf,
                }
            }).then(
                function () {
                    vm.status = 'Main'
                    vm.elements = 'Пост создан'
                })
        },
    },
    created: function () {
        const vm = this;
        vm.elements = "Главная страница"
        vm.status = 'Main'
    }
})

